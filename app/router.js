import Home from './components/Home';
import Settings from './components/Settings';
import FirstPage from './components/Splashscreen';
import Service from './components/Service';
import Scan from './components/Scan';
import Ratestar from './components/Ratestar';
import Ratecomment from './components/Ratecomment';
import Thank from './components/Thank';

let routes = [
  {
    path: '/home',
    component: Home
  },
  {
    path: '/settings',
    component: Settings
  },
  {
    path: '/firstPage',
    component: FirstPage
  },
  {
    path: '/service',
    component: Service
  },
  {
    path: '/scan',
    component: Scan
  },
  {
    path: '/ratestar',
    component: Ratestar
  },
  {
    path: '/ratecomment',
    component: Ratecomment
  },
  {
    path: '/thank',
    component: Thank
  }
];

function getRoute(path) {
  const dedicated = routes.filter(route => {
    return route.path == path;
  });

  if (dedicated.length > 0) {
    return dedicated[0].component;
  }

  return Home;
}

export default function (Vue) {
  this.push = function (path, params) {
    Vue.prototype.$navigateTo(getRoute(path), {
      props: params,
    });
  }
  this.pushnotrace = function (path, params) {
    Vue.prototype.$navigateTo(getRoute(path), {
      backstackVisible: false,
      props: params,
      clearHistory: true
    });
  }

  this.replace = function (path, params) {
    Vue.prototype.$navigateTo(getRoute(path), {
      props: params,
      clearHistory: true
    });
  }

  this.back = function () {
    Vue.prototype.$navigateBack();
  }

  this.getComponent = function (name) {
    return routes.find(route => route.meta.title == name);
  }

  return this;
};