import Vue from 'nativescript-vue'
import FirstPage from './components/Splashscreen';
import Router from './router';
import service from './service'
import config from './config'
import axios from 'axios'
import VueAxios from 'vue-axios'

import fontList from './font-awesome';

Vue.config.silent = (TNS_ENV === 'production')

Vue.filter('fonticon', function (value) {
  return fontList[value];
});

Vue.use(VueAxios, axios);

let router = new Router(Vue);

Vue.prototype.$router = router;

Vue.prototype.$service = service(axios, config);

new Vue({
  render: h => h('frame', [h(FirstPage)])
}).$start()


Vue.registerElement('BarcodeScanner', () => require('nativescript-barcodescanner').BarcodeScannerView);

const appSettings = require("tns-core-modules/application-settings");

function setUser(){
  appSettings.setString("clientId", 'data')
}
exports.setUser = setUser;
function getUser(){  
  console.log(appSettings.getString("clientId"))
}
exports.getUser = getUser;
function ifexist(){
  return appSettings.hasKey("clientId");
}
exports.ifExist = ifexist;

Vue.prototype.$ifExt = ifexist;
Vue.prototype.$setUse = setUser;
Vue.prototype.$getUse = getUser;

function onNavigatingTo(args) {
    appSettings.setBoolean("isTurnedOn", true);
    appSettings.setString("username", "Wolfgang");
    appSettings.setNumber("locationX", 54.321);

    const isTurnedOn = appSettings.getBoolean("isTurnedOn");
    const username = appSettings.getString("username");
    const locationX = appSettings.getNumber("locationX");

    // Will return "No string value" if there is no value for "noSuchKey"
    const someKey = appSettings.getString("noSuchKey", "No string value");

    // Will return false if there is no key with name "noSuchKey"
    const isKeExisting = appSettings.hasKey("noSuchKey");
}
exports.onNavigatingTo = onNavigatingTo;

function onClear() {
    // Removing a single entry via its key name
    appSettings.remove("isTurnedOn");

    // Clearing the whole application-settings for this app
    appSettings.clear();
}