export default function ($http, $config) {

  var get = function (endpoint, params, callback) {
    return $http({
      method: 'get',
      url: $config.apiURL + endpoint,
      params
    })
      .then((response) => {
        if (response.data) {
          callback(null, response.data);
        } else {
          callback('Error');
        }
      }).catch((error) => {
        callback(error);
      });
  };

  var post = function (endpoint, params, callback) {
    return $http({
      method: 'post',
      url: $config.apiURL + endpoint,
      data: params
    })
      .then((response) => {
        if (response.data) {
          callback(null, response.data);
        } else {
          callback('Error');
        }
      }).catch((error) => {
        callback(error);
      });
  };


  return {
    get,
    post
  }
};